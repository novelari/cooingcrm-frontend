import React from 'react';
import EditableText from '../../components/EditableText/index';
import EditableDropdown from '../../components/EditableDropdown/index';
import EditableCreatable from '../../components/EditableCreatable/index';
import Assign from '../../Assign';
import LeadStatus from '../../LeadStatus'

export class RightSideView extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={"col-xs-12 col-md-3 lead-container-box"}>

        <div className={"lead-field"}>
          <label style={{ marginBottom: "5px" }}> Status: </label>
          <EditableCreatable
            field={"status_id"}
            value={this.props.statuses.length > 0 && this.props.leadData.status_id && this.props.statuses.filter((status)=>this.props.leadData.status_id == status.value)[0].label}
            validate={() => false}
            options={this.props.statuses}
            onSave={this.props.onSave}
            customClass={`alert alert-${LeadStatus[this.props.leadData.status] || "default"} custom-alert`}
            onCreateNewOption={this.props.createNewStatus}
          />
        </div>
        <div className={'lead-field'}>
          <label>Buyer/seller: </label>
          <EditableDropdown
            field='buyer_seller'
            value={this.props.leadData['buyer'] ? 'Buyer' : 'Seller'}
            validate={() => false}
            options={["Buyer", "Seller"]}
            onSave={this.props.onSave}
          />
        </div>
        <div className={"lead-field"}>
          <label> Assigned To: </label>
          <Assign assignTo={this.props.leadData.assignTo} leadId={this.props.leadData.id} />
        </div>

      </div>
    );
  }
}

export default RightSideView;
