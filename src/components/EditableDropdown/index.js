import React from 'react';
import { EditableComponent } from '../EditableComponent/index';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

export class EditableDropdown extends EditableComponent {
  constructor(props) {
    super(props);
  }

  renderEdit() {
    return (
      <div className={""}>
        <Select
          name="form-field-name"
          value={this.state.value}
          options={this.props.options.map((val, i) => { return { value: val, label: val }} )}
          onChange={this.handleValueChange}
          searchable={true}
          clearable={false}
        />
        {this.renderEditOptions()}
      </div>
    );
  }

  renderEditOptions() {
    return (
      <div className={"text-right"}>
        <button type="button" className="btn btn-danger" onClick={() => this.cancelChanges()}>
          <i className="fa fa-times" aria-hidden="true"></i>
        </button>
        <button type="button" className="btn btn-primary" onClick={() => this.saveChanges()}>
          <i className="fa fa-check" aria-hidden="true"></i>
        </button>
      </div>
    );
  }

  handleValueChange(obj) {
    this.setState({ value: obj.value });
  }

}

export default EditableDropdown;
