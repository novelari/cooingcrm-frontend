import React from 'react';
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import LeadDetails from '../containers/LeadDetails/index.jsx';
import LeftSideView from '../containers/LeadDetails/LeftSideView';
import moxios from 'moxios';
import API from '../api/index';

jest.mock('universal-cookie', () => function Cookie() {
    this.get = (s) => {
        if (s == 'userId') { return 1 }
        else if (s == 'auth_token') { return 'fdfddfdasdfdsf' }
        else if (s == 'userRoles') {return [{role:"sales_manager"}]}
    }
    this.set = ()=>{}
})


const lead = {
    name: 'Ahmed',
    telephone: '0123456677',
    date_of_first_contact: '20-10-2016',
    method_of_contact: 'telephone',
    unit_value_currency: 'EGP',
    status: 'Needs to be contacted',
    budget: 1000,
    budget_currency: 'EGP',
}


describe('Lead details component', () => {


    test('should update state with details from  backend', (done) => {

        //attached to document.body because document.getElementById is used inside the component
        moxios.install();

        const component = mount(<LeadDetails match={{ params: { id: 3 } }} />)

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: { lead: lead }
            }).then((response) => {

                expect(component.instance().state.leadData).toEqual(lead);
                done();
            })
        })
    })

    test('updateLead should update state.leadData ', (done) => {

        moxios.install();

        const component = mount(<LeadDetails match={{ params: { id: 3 } }} />)

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: { lead: lead }
            }).then((response) => {

                //update irectly by calling the function updateLead()
                component.instance().updateLead('name', 'Karim');
                expect(component.instance().state.leadData.name).toEqual('Karim');

                //update by updating editable component
                const budget = component.findWhere(node => node.props().field === "budget");
                budget.node.setState({ value: 2000 })
                budget.node.saveChanges()
                expect(component.instance().state.leadData.budget).toEqual(2000)
                done();
            })
        })

    })

    test('lead shouldnt update if didnt pass validation', (done) => {

        moxios.install();

        const component = mount(<LeadDetails match={{ params: { id: 3 } }} />)

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: { lead: lead }
            }).then((response) => {

                //update by updating editable component
                const budget = component.findWhere(node => node.props().field === "budget");
                budget.node.setState({ value: -2000 })
                budget.node.saveChanges()
                expect(component.instance().state.leadData.budget).toEqual(1000)
                done();
            })
        })

    })
    
    describe('LeftSideView', ()=> {

        const component = mount(<LeftSideView leadData={lead} onSave={()=>{}}/>);

        describe('validate telephone', ()=>{

            const validateTelephone = component.instance().validateTelephone;

            test('should return "Telephone number must be more than 4 numbers" if no. has less than or equal 4 digits', ()=>{
                expect(validateTelephone('021')).toEqual('Telephone number must be more than 4 numbers');
            })
             test('should return "" if no. has more than 4 digits', ()=>{
                expect(validateTelephone('02123')).toEqual('');
            })
        })


        describe('validate email', ()=>{

            const validateEmail = component.instance().validateEmail;

            test('should return "Not a valid email addres" if email doesnt have @ or 2 characters after .', ()=>{
                expect(validateEmail('ahmed.com')).toEqual('Not a valid email addres');
                expect(validateEmail('ahmed@gmail.c')).toEqual('Not a valid email addres');
                
            })
             test('should return "" if email is valid', ()=>{
                expect(validateEmail('ahmed@gmail.com')).toEqual('');
                 
            })
        })
    })




})