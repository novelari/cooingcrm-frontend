import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const userCanAddLead = () => {
    let userRoles = cookies.get('userRoles').map((role) => role.role);
    if (userRoles.indexOf('sales_manager') >= 0 || userRoles.indexOf('data_entry') >= 0) {
      return true;
    }
    return false
  }


const NavbarHeader = () => (
  <div className="navbar-header">
    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span className="sr-only">Toggle navigation</span>
      <span className="icon-bar"></span>
      <span className="icon-bar"></span>
      <span className="icon-bar"></span>
    </button>
    <Link className="navbar-brand" to="/">
      <img src="http://www.cooingestate.com/assets/mobile/img/logo.png" /> CRM
    </Link>
  </div>
);

const NavbarCollapse = (props) => (
  <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      {(cookies.get('auth_token')) ? (
        <ul className="nav navbar-nav">
        <li>
          <Link to="/leads">LEADS</Link>
        </li>
        <li>
          {(cookies.get('userRoles') && userCanAddLead())?
          <Link to="/leads/new">ADD NEW LEAD</Link>:<p></p>}
        </li>
      </ul>
      ) : <p></p>

      }
    <ul className="nav navbar-nav navbar-right">
      {(props.logoutEnabled)?(
          (cookies.get('auth_token'))?
          (<li>
            <a onClick={() => {cookies.remove('auth_token')}} href="/">LOGOUT</a>
          </li>) : (<li><Link to='/'>LOGIN</Link></li>)) : <li></li>
      }
      {/* <li><a href="#">Link</a></li> */}
    </ul>
  </div>
);

export class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-default cooing-navbar">
        <div className="container-fluid">
          {/* <!-- Brand and toggle get grouped for better mobile display --> */}
          <NavbarHeader />

          {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
          <NavbarCollapse logoutEnabled={this.props.logoutEnabled}/>
          {/* <!-- /.navbar-collapse --> */}
        </div>
        {/* <!-- /.container-fluid --> */}
      </nav>
    );
  }
}

export default Navbar;
