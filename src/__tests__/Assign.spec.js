import React from 'react';
import moxios from 'moxios'
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import Assign from '../Assign'

jest.mock('universal-cookie', () => function Cookie() {
    this.get = (s) => {
        if (s == 'userId') { return 1 }
        else if (s == 'auth_token') { return 'fdfddfdasdfdsf' }
        else if (s == 'userRoles') {return [{role:"sales_manager"}]}
    }
    this.set = ()=>{}
})

const users = [{ "id": 1, "name": "karim", "email": "karim@gmail.com" },
{ "id": 2, "name": "medhat", "email": "medhat@gmail.com" },
{ "id": 3, "name": "ahmed", "email": "ahmed@gmail.com" }]


describe('Assign component', () => {


    it('getUsers should update state.users from backend', (done) => {

        moxios.install();

        const component = mount(<Assign leadId={1} />);
        component.instance().getAgents();

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: users
            }).then(() => {
                expect(component.instance().state.users).toEqual(users)
                done();
            })
        })
    })

     it('updateAssigned should update state.assignTo to user\'s  email', (done) => {

        moxios.install();

        const component = mount(<Assign leadId={1} />);
        component.instance().getAgents();

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: users
            }).then(() => {
                deleteCallBack()
                
            })
        })

        function deleteCallBack(){
            const assign = component.instance().assign;
            assign({},'karim@gmail.com');

            moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
            }).then(() => {
                expect(component.instance().state.assignTo).toEqual('karim@gmail.com')
                done();
            })
        })

        }
    })

})


