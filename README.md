#Dependencies

[nodejs](https://nodejs.org/en/)

[git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

#Installation

Clone the repo and install dependencies:

$ git clone https://bitbucket.org/novelari/cooingcrm-frontend
$ cd cooingcrm-frontend
$ npm install

#To build the app:

$ npm run build:vender
$ npm run build

#To run on local server (webpack-dev-server):

$ npm run server

#To run tests :

$ npm run test

#To deploy on netlify:
 just push your changes to development branch if you want to deply the staging version
 or on the master branch to deploy the production version


