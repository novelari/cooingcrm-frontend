import React from 'react';
import moxios from 'moxios'
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import Alerts from '../containers/LeadDetails/Alerts.jsx';

jest.mock('universal-cookie', () => function Cookie() {
    this.get = (s) => {
        if (s == 'userId') { return 1 }
        else if (s == 'auth_token') { return 'fdfddfdasdfdsf' }
        else if (s == 'userRoles') {return [{role:"sales_manager"}]}

    }
})

const alerts = [{
    "id": 1,
    "date": "2017-08-28T14:00:43.387Z",
    "desc": "alert for lead 11 on 4 pm",
    "user_id": 41,
    "created_at": "2017-08-28T13:58:25.685Z",
    "updated_at": "2017-08-28T13:58:25.685Z",
    "lead_id": 11
}, {
    "id": 2,
    "date": "2017-08-28T14:30:10.484Z",
    "desc": "new alert for testing heroku worker",
    "user_id": 41,
    "created_at": "2017-08-28T14:28:07.462Z",
    "updated_at": "2017-08-28T14:28:07.462Z",
    "lead_id": 11
}, {
    "id": 3,
    "date": "2017-08-28T14:38:19.941Z",
    "desc": "another alert on 4:38 pm",
    "user_id": 41,
    "created_at": "2017-08-28T14:37:04.950Z",
    "updated_at": "2017-08-28T14:37:04.950Z",
    "lead_id": 11
}]

let component;

beforeAll((done) => {

    moxios.install();

    component = mount(<Alerts leadId={1} />);

    moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
            status: 200,
            response: alerts
        }).then(() => {
            done();
        })
    })


})

describe('Alerts component', () => {


    it('should update state.schedules from backend when component loads', () => {
        expect(component.instance().state.schedules).toEqual(alerts)
    })

    it('create alert should send the right data to the backend', (done) => {

        //moxios.install();

        //const component = shallow(<Alerts leadId={1} />);

        const createAlert = component.instance().createAlert;
        const setDescription = component.instance().setDescription;
        const event = { target: { value: 'hello' } }
        setDescription(event);

        const setDate = component.instance().handleChange;
        setDate('10/10/2017');


        createAlert();

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            const expectedData = { date: "10/10/2017", lead_id: 1, user_id: 1, desc: "hello", notify_assigned_lead: true }
            expect(JSON.parse(request.config.data)).toEqual(expectedData)
            done();
        })

    })

    it ('clicking notify assigned lead should change state.notifyAssignedLead', ()=>{

         component.find('input[name="notify-assigned"]').simulate('change',{target: {checked: false}})
        
        expect (component.instance().state.notifyAssignedLead).toBe(false)
    })

    it('delete alert should delete alert from state.scehdules', (done) => {

        const deleteAlert = component.instance().deleteAlert;

        deleteAlert(2);

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200
            })
                .then(() => {
                    const schedules = component.instance().state.schedules;
                    expect(schedules.length).toEqual(2);
                    const deleted = schedules.filter(s => s.id == 2)[0];
                    expect(schedules.indexOf(deleted)).toBe(-1)
                    done();
                })

        })
    })
})


