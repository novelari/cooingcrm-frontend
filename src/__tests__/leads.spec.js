import React from 'react';
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import Leads from '../containers/Leads/Leads.jsx';
import moxios from 'moxios';
import { getLeads } from '../api/asyncActions';
import API from '../api/index';
import { deleteLead } from '../api/asyncActions';

jest.mock('universal-cookie', () => function Cookie() {
    this.get = (s) => {
        if (s == 'userId') { return 1 }
        else if (s == 'auth_token') { return 'fdfddfdasdfdsf' }
        else if (s == 'userRoles') {return [{role:"sales_manager"}]}
        // else if (s == 'columns_to_show') {return 'id, name'}
    }
    this.set = ()=>{}
})

const lead = { name: 'ahmed' }

const data ={leads: [
    {
        "id": 20,
        "status": "Waiting on new launch",
        "name": "ahmed",
        "date_of_first_contact": "2016-12-31",
        "method_of_contact": "telphone",
        "telephone": "0102020323",
        "email": "a@ds.cu",
        "buyer": true,
        "seller": false,
        "best_time_to_call": "12-5 pm",
        "desc": "any description]",
        "budget": 3223,
        "date_of_trans": "2018-01-01",
        "what_was_bought": "apart123",
        "unit_value": 34554,
        "commission_earned": null,
        "created_at": "2017-08-29T14:47:50.910Z",
        "updated_at": "2017-09-10T11:13:27.388Z",
        "user_id": 22,
        "LeadSource": null,
        "LeadSubSource": null,
        "LstTimeFOF": null,
        "FrstTimeReqTbContacted": null,
        "LstReQTbContacted": null,
        "budget_currency": "EGP",
        "unit_value_currency": "EGP"
    },
    {
        "id": 21,
        "status": "Meeting set up",
        "name": "menem",
        "date_of_first_contact": "2016-12-31",
        "method_of_contact": "email",
        "telephone": "0100222222",
        "email": "hossam@gmail.com",
        "buyer": false,
        "seller": true,
        "best_time_to_call": "after 8 pm",
        "desc": "description",
        "budget": 1500000,
        "date_of_trans": "2018-02-01",
        "what_was_bought": "apartment",
        "unit_value": 199999,
        "commission_earned": null,
        "created_at": "2017-08-29T14:48:28.561Z",
        "updated_at": "2017-09-10T10:04:17.885Z",
        "user_id": 12,
        "LeadSource": null,
        "LeadSubSource": null,
        "LstTimeFOF": null,
        "FrstTimeReqTbContacted": null,
        "LstReQTbContacted": null,
        "budget_currency": "EGP",
        "unit_value_currency": "EGP"
    }
],
meta: {total_count:2, limit: 10}};


var component;

beforeAll((done) => {

    moxios.install();
    moxios.wait(function () {

        // let request = moxios.requests.mostRecent();
        let statusesRequest = moxios.requests.at(0);
        let propertyTypesRequest = moxios.requests.at(1);
        let searchRequest = moxios.requests.at(2);
        let agentsRequest = moxios.requests.at(3);

        statusesRequest.respondWith({
            status:200,
            response: [{status:"no answer"}]
        }).then(function(){
            done();
        })

        propertyTypesRequest.respondWith({
            status:200,
            response: [{property_type:"apartment"}]
        }).then(function(){
            done();
        })   

        agentsRequest.respondWith({
            status:200,
            response: [{name:"ahmed"}]
        }).then(function(){
            done();
        })
        

        searchRequest.respondWith({
            status: 200,
            response: data
        }).then(function () {
            done();
        })

        
    })

   


    component = mount(<Leads />);

})

describe('Leads component', () => {



    test('Leads renders with correct number of leads', () => {


        expect(component.instance().state.leads.length).toEqual(2);

    })

    
    test('should show selected columns only', () => {

        //default is 9 columns
        expect(component.find('thead th').find({style:{display: 'table-cell'}}).length).toBe(9);

        const handleColumnSelection = component.instance().handleColumnSelection;
        handleColumnSelection({target:{name:'name',checked:false}});
        //check that 1 column is hidden
        expect(component.find('thead th').find({style:{display: 'table-cell'}}).length).toBe(8);
        
    })

})