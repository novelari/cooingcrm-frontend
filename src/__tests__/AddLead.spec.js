import React from 'react';
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import AddLead from '../AddLead';
import moxios from 'moxios';
import { getLeads } from '../api/asyncActions';
import API from '../api/index';
import { deleteLead } from '../api/asyncActions';

const lead = {
    name: 'Ahmed',
    telephone: '0123456677',
    date_of_first_contact: '20-10-2016',
    method_of_contact: 'telephone',
    unit_value_currency: 'EGP',
    validationErrorMessages: [],
    budget_currency: 'EGP',

}


const statuses = ["Needs to be contacted",
    "Following up",
    "No Answer",
    "Stopped Answering",
    "Wrong Number",
    "Not Interested",
    "Low Budget",
    "Bought from someone else"]

let component;

beforeAll((done) => {

    moxios.install();

    component = mount(<AddLead leadId={1} />);

    moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
            status: 200,
            response: statuses
        }).then(() => {
            done();
        })
    })


})


describe('Add lead component', () => {


    test('should change state on submit with the right data', (done) => {


        let name = component.find('input [name="name"]')
        name.first().simulate('change', { target: { value: 'Ahmed', name: "name" } })

        let telephone = component.find('input [name="telephone"]')
        telephone.simulate('change', { target: { value: '0123456677', name: "telephone" } })

        let dateOfFirstContact = component.find('input [name="date_of_first_contact"]')
        dateOfFirstContact.simulate('change', { target: { value: '20-10-2016', name: "date_of_first_contact" } })

        let methodOfContact = component.find('input [name="method_of_contact"]')
        methodOfContact.simulate('change', { target: { value: 'telephone', name: "method_of_contact" } })

        moxios.install();
        moxios.wait(function () {

            let request = moxios.requests.at(2);
            request.respondWith({
                status: 200,
                response: { data: ['error1'] }
            }).then(() => {
                expect(component.instance().state.lead).toEqual(lead);
                done();
            })
        })


        const form = component.find('#add-new-lead-form');
        const fakeEvent = { preventDefault: () => {} };
        form.simulate('submit', fakeEvent)


    })

     test('should show a span when successfuly create new lead', (done) => {


        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                
            }).then(() => {
                expect(component.instance().state.successAdd).toEqual(true);
                expect(component.find('div .alert .alert-success').exists()).toBe(true)
                done();
            })
        })


        const form = component.find('#add-new-lead-form');
        const fakeEvent = { preventDefault: () => {} };
        form.simulate('submit', fakeEvent)

    })

    test('should append error messages from backend to state.validationErrorMessages', (done) => {


        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 422,
                response: {email:['wrong email format']}
            }).then(() => {
                expect(component.instance().state.validationErrorMessages).toEqual(['email : wrong email format']);
                done();
            })
        })


        const form = component.find('#add-new-lead-form');
        const fakeEvent = { preventDefault: () =>{} };
        form.simulate('submit', fakeEvent)

    })

    test('should show error message from backend as a div', (done) => {


        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 422,
                response: {email:['wrong email format']}
            }).then(() => {
                expect(component.find('div.alert.alert-danger').getNode().innerHTML).toEqual('email : wrong email format')
                done();
            })
        })


        const form = component.find('#add-new-lead-form');
        const fakeEvent = { preventDefault: () => {} };
        form.simulate('submit', fakeEvent)

    })




})