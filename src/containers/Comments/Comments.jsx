import React from 'react';
import Comment from './Comment';
import axios from 'axios';
import Cookies from 'universal-cookie';
import API from '../../api/index.js';


const cookies = new Cookies();
const userName = cookies.get('name');

// should take lead id in props as "leadId"
export class Comments extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            commentSending:false,
            comments:[],
            commentToSend: ""
        }
        this.handleValueChange = this.handleValueChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.addComment = this.addComment.bind(this);
        this.deleteComment = this.deleteComment.bind(this);
    }
    componentDidMount(){
        axios({
            method: 'GET',
            url: API.BASE + `/api/leads/${this.props.leadId}/comments`,
            headers: { Authorization: cookies.get('auth_token') },
        })
            .then((response) => {
                this.setState({ comments: response.data });
            });
    }
    addComment(){
        this.setState({commentSending: true})
        let comment = {
            message: this.state.commentToSend,
            lead_id: this.props.leadId,
            user_id: cookies.get('userId')
        }
         axios({
            method: 'POST',
            url: API.BASE + `/api/leads/${this.props.leadId}/comments`,
            headers: { Authorization: cookies.get('auth_token') },
            data: comment
        })
            .then((response) => {
                this.setState(prevState=>{prevState.comments.push(response.data);prevState.commentSending = false})
            });
    }
    deleteComment(comment){
        axios({
            method: 'delete',
            url: API.BASE + `/api/leads/${this.props.leadId}/comments/` + comment.id,
            headers: { Authorization: cookies.get('auth_token') },
        }).then((response) => {
            this.setState((prevState) => {prevState.comments.splice(prevState.comments.indexOf(comment),1)})
            
        });

    }
    handleKeyDown(event){
        if (event.keyCode === 13) {
             this.addComment();
             event.target.value = ""
        }
    }
    handleValueChange(event){
        this.setState({
            commentToSend: event.target.value
        })
    }

  render() {
      return (
        <div id="comments-container">
            <h3>Comments</h3>
            {(this.state.comments.length <= 0)? 
                <h4>No comments</h4> :
                this.state.comments.map((comment) => 
                        <Comment 
                            key={comment.id} 
                            comment={comment}
                            currentUserId={cookies.get('userId')} 
                            onDelete={() => this.deleteComment(comment)}/>)
            }
            <textarea 
                placeholder="Write comment.."
                className="form-control comment-new"
                onKeyDown={this.handleKeyDown}
                onChange={this.handleValueChange}
                disabled={this.state.commentSending}
                />
            {this.state.commentSending?
                <div style={{position:'relative',bottom:'40px',right: '20px', textAlign:'center'}}>
                    <i className="fa fa-spinner fa-spin fa-lg fa-fw"></i>
                </div> :''
            }
        </div>

      )
  }
}

export default Comments;
