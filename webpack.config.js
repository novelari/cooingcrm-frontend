const path = require('path');
const webpack = require('webpack');

const ExtractTextPlugin   = require('extract-text-webpack-plugin');
const autoprefixer        = require('autoprefixer');
const HtmlWebpackPlugin   = require('html-webpack-plugin');

const distDir = path.join(__dirname, 'dist');
const srcDir  = path.join(__dirname, 'src');

module.exports = {
  target: 'web',
  entry: {
    index: `${srcDir}/ClientApp.jsx`, // index.js
  },
  output: {
    path: distDir,
    filename: '[name].bundle.js',
    publicPath: '/', // public path is necessary for react routing to work
  },
  module: {
    rules: [
      // {
      //   enforce: 'pre',
      //   test: /\.jsx?$/,
      //   loader: 'eslint-loader',
      //   exclude: /node_modules/,
      // },
      {
        test: /\.(css|scss|sass)$/,
        use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [ autoprefixer ],
              },
            },
            'sass-loader',
          ]
        })),
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ 'es2015', 'react' ],
          },
        },
      },
      {
        test: /\.eot|woff2|woff|ttf|svg/,
        loader: 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.json',
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'index.html'),
      // template: path.join(__dirname, 'src', 'index.html'),
      inject: 'body',
      filename: 'index.html',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin("style.css"),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV':JSON.stringify(process.env.NODE_ENV || "development"),
    }),
  ],
  devServer: {
    hot: true,
    inline: true,
    contentBase: distDir,
    // publicPath: '/dist/',
    historyApiFallback: true,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  devtool: "source-map",
};
