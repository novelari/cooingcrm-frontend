import React from 'react';
import LeadFields from '../../LeadFields.js';


export class TableOptions extends React.Component {
    constructor(props) {
        super(props);
    }
    isChecked(field) {
        if (this.props.selectedColumns.indexOf(field) >= 0) {
            return true;
        } else {
            return false;
        }
    }

    humanize(str) {
        var frags = str.split('_');
        for (let i = 0; i < frags.length; i++) {
            frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
        }
        return frags.join(' ');
    }


    render() {
        return (
            <div className="pull-right" id="table-options">
                <h4>Columns to show:</h4>
                <ul >

                    {this.props.columns.map((field, index) => (
                       <li key={index}>
                            <div  className="custom-checkbox">
                                <input checked={this.isChecked(field)} type="checkbox" name={field} id={field} onChange={this.props.handleSelection} />
                                <label htmlFor={field}>{this.humanize(field)}</label>
                            </div>
                       </li>
                    ))}
                    
                </ul>
                <div style={{borderTop:'1px solid lightgray', margin: '15px 10px', paddingTop: '10px'}}>
                        <div className="custom-checkbox">
                            <input checked={this.props.archivedVisibility} type="checkbox" name={'archived-check'} id={'archived-check'} onChange={this.props.toggleShowArchived} />
                            <label htmlFor={'archived-check'}>Show Archived</label>
                        </div>
                    </div>
            </div>
        )
    }
}

export default TableOptions;
