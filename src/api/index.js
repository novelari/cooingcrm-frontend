let BASE;

if (process.env.NODE_ENV == "development"){
  BASE =`${location.protocol}//localhost:3000`;
} else if (process.env.NODE_ENV == "staging"){
  BASE = `${location.protocol}//cooing-crm-staging.herokuapp.com`;
} else if (process.env.NODE_ENV == "production"){
  BASE = `${location.protocol}//cooing-crm.herokuapp.com`;
}

export default {
  BASE,
};
