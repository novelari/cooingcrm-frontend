import React from 'react';
import PropTypes from 'prop-types';
import { EditableComponent } from '../EditableComponent/index';

export class EditableTextarea extends EditableComponent {
  renderEdit() {
    return (
      <div className="editable-textarea">
        <textarea className={`form-control ${this.state.validationError?'error-input':''}`}
          style={{
            resize: 'vertical',
            width: "100%",
          }}
          rows="6"
          onKeyDown={this.handleKeyDown}
          value={this.state.value}
          onChange={this.handleValueChange}>
        </textarea>
        <div className={"text-right"}>
            <i onClick={() => this.cancelChanges()} className="fa fa-times fa-lg" aria-hidden="true"></i>
            <i onClick={() => this.saveChanges()} className="fa fa-check fa-lg" aria-hidden="true"></i>
            {(this.state.validationError)? <small id="error-validation">{this.state.validationError}</small> : <p></p>}

        </div>
      </div>
    );
  }

}

export default EditableTextarea;
