import React from 'react';
import moxios from 'moxios'
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import Comments from '../containers/Comments/Comments.jsx'

jest.mock('universal-cookie', () => function Cookie() {
    this.get = (s) => {
        if (s == 'userId') { return 1 }
        else if (s == 'auth_token') { return 'fdfddfdasdfdsf' }
    }
})

const comments = [{
    "id": 1,
    "message": "commenting...",
    "created_at": "2017-08-28T13:56:39.844Z",
    "updated_at": "2017-08-28T13:56:39.844Z",
    "user_id": 41, "lead_id": 11,
    "commenter_name": "ahmed",
    "commenter_id": 41,
    "commenter_email": "okashoon@gmail.com"
},
{
    "id": 2,
    "message": "hellooo",
    "created_at": "2017-08-28T13:57:50.995Z",
    "updated_at": "2017-08-28T13:57:50.995Z",
    "user_id": 41,
    "lead_id": 11,
    "commenter_name": "ahmed",
    "commenter_id": 41,
    "commenter_email": "okashoon@gmail.com"
}, {
    "id": 3,
    "message": "comment",
    "created_at": "2017-08-29T09:48:34.792Z",
    "updated_at": "2017-08-29T09:48:34.792Z",
    "user_id": 43,
    "lead_id": 11,
    "commenter_name": "ahmed",
    "commenter_id": 43,
    "commenter_email": "ahmed@gmail.com"
}]


let component;

beforeAll((done) => {

    moxios.install();

    component = mount(<Comments leadId={1} />);

    moxios.wait(function () {
        let request = moxios.requests.mostRecent();
        request.respondWith({
            status: 200,
            response: comments
        }).then(() => {
            done();
        })
    })


})

describe('Comments component', () => {


    it('should update state.comments from backend when component loads', () => {
        expect(component.instance().state.comments).toEqual(comments)
    })

    it('addComment should send the right data to the backend', (done) => {

        const addComment = component.instance().addComment;
        const handleValueChange = component.instance().handleValueChange;
        const event = { target: { value: 'comment for test', keyCode:13 } }
        handleValueChange(event);

        const handleKeyDown = component.instance().handleKeyDown;
        handleKeyDown(event);

        addComment();

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            const expectedData = {"message":"comment for test","lead_id":1,"user_id":1}
            expect(JSON.parse(request.config.data)).toEqual(expectedData)
            done();
        })
    })

    

    it('delete comment should delete alert from state.comments', (done) => {

        const comments = component.instance().state.comments;
        

        const deleteComment = component.instance().deleteComment;
        const commentToBeDeleted = comments.filter(comment => comment.id == 2);

        deleteComment(commentToBeDeleted);

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200
            })
                .then(() => {
                    expect(comments.length).toEqual(2);
                    expect(comments.indexOf(commentToBeDeleted)).toBe(-1)
                    done();
                })

        })
    })
})


