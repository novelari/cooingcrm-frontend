import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';
import API from './api/index';
import EditableDropdown from './components/EditableDropdown/index';
import { getAgents } from './api/asyncActions.js'

const cookies = new Cookies();

class Assign extends Component {
  constructor(...props) {
    super(...props);
    this.state = {
      leads: [],
      users: [],
      usersEmails:[],
      allLeads: [],
      user: [],
      UserRole: '',
      assignTo: this.props.assignTo
    };

    this.getAgents = this.getAgents.bind(this);
    this.assign = this.assign.bind(this);
  }


  assign(event, email) {
    var selectedUser = this.state.users.filter((u) => u.email == email)[0]
    this.setState({assignTo: selectedUser.email})
    axios({
      method: 'PUT',
      url: API.BASE + '/api/leads/'+this.props.leadId +'/AssignUser',
      data: {
        user_id: selectedUser.id,
        manager_id: cookies.get('userId')
      },
      headers: { Authorization: cookies.get('auth_token') }
    })
    .then(response => {
      this.setState({assignTo: selectedUser.email})
    });
  }

  // to get email and name of user who is sales_agent
  getAgents(event) {
    !this.state.users.length &&
    getAgents().then(response => {
      let usersEmails = [];
      for(let user of response.data){
        usersEmails.push(user.email);
      }
      this.setState({ users: response.data,usersEmails: usersEmails });
    });
  }

  render() {
    return (
      <div onClick={event => {
          this.getAgents(event);
        }}>
        <EditableDropdown
          field={'assign-to'}
          value={this.state.assignTo || this.props.assignTo || "Unassigned"}
          validate={() => false}
          options={this.state.usersEmails}
          onSave={this.assign}
        />
      </div>
    );
  }
}

export default Assign;

/*
 <div className="container">
        <div className="dropdown">
          <button
            disabled={this.state.allLeads.user_id}
            type="button"
            data-toggle="dropdown"
            className={this.state.condition ? 'btn btn-primary dropdown-toggle' : 'alert alert-primary'}
            onClick={event => {
              this.handleSubmit(event);
            }}
          >
          {!this.props.user ? 'Unassigned' : 'Assigned to ' + this.props.user}
          </button>
          <ul className="dropdown-menu">
            {this.state.users.map(u =>
              <li
                className="btn btn-primary"
                onClick={event => {
                  this.assign(event, u.id, u.name);
                }}
              >
                name : {u.name} -
                email : {u.email}
              </li>
            )}
          </ul>
        </div>
      </div>
*/
