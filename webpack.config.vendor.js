const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const distDir = path.join(__dirname, 'dist');
const srcDir  = path.join(__dirname, 'src');

module.exports = {
  stats: {
    modules: false,
  },
  resolve: {
    extensions: [ '.js' ],
  },
  entry: {
    vendor: [
      'bootstrap/dist/css/bootstrap.css',
      'font-awesome/css/font-awesome.css',
      'jquery',
      'bootstrap/dist/js/bootstrap.js',
      'prop-types',
      'react',
      'react-axios',
      'react-dom',
      'react-redux',
      'react-router-dom',
      'redux',
      'redux-thunk',
    ],
  },
  output: {
    path: distDir,
    publicPath: '/dist/',
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.css?$/,
        use: ExtractTextPlugin.extract({
          use: 'css-loader',
        }),
      },
      {
        test: /\.(png|jpg|jpeg|woff|woff2|eot|ttf|svg|gif)(\?|$)/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 25e3
          }
        }
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('vendor.css'),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      jquery: 'jquery'
    }),
  ],
};
