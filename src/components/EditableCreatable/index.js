import React from 'react';
import { EditableComponent } from '../EditableComponent/index';
import SelectWithAdd from '../../components/SelectWithAdd.jsx'
import 'react-select/dist/react-select.css';

export class EditableCreatable extends EditableComponent {
  constructor(props) {
    super(props);
    this.createNewOption = this.createNewOption.bind(this)
  }

  createNewOption(option){
    //wait until new option is created
    this.props.onCreateNewOption(option).then((response)=>{
      
      this.setState({options: this.props.options})
      
    })
  }

  

  renderEdit() {
    return (
      <div className={""}>
        <SelectWithAdd 
                  value={this.state.value}
                  options={this.state.options}
                  onChange={this.handleValueChange}
                  createNewOption={this.createNewOption}
                />
        {this.renderEditOptions()}
      </div>
    );
  }

  renderEditOptions() {
    return (
      <div className={"text-right"}>
        <button type="button" className="btn btn-danger" onClick={() => this.cancelChanges()}>
          <i className="fa fa-times" aria-hidden="true"></i>
        </button>
        <button type="button" className="btn btn-primary" onClick={() => this.saveChanges()}>
          <i className="fa fa-check" aria-hidden="true"></i>
        </button>
      </div>
    );
  }

  handleValueChange(obj) {
    this.setState({ value: obj.value });
  }

}

export default EditableCreatable;
