import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
import API from './api/index';
import './containers/Login/index.sass';

const cookies = new Cookies();

class Login extends Component {
  constructor(...props) {
    super(...props);
    this.email = '';
    this.password = '';
    this.setEmail = this.setEmail.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setNewPassword = this.setNewPassword.bind(this);
    this.assignNewPasswordToState = this.assignNewPasswordToState.bind(this);
    this.state ={
      isLoggedIn: true,
      waitingForLogging: false,
      firstTime:false,
      userId: 0,
      newPassword: ''
    }
  }

  setEmail(event) {
    this.email = event.target.value;
  }
  setPassword(event) {
    this.password = event.target.value;
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({waitingForLogging: true})
    axios({
      method: 'POST',
      //chng it to cooing-crm
      url: API.BASE + `/authenticate`,
      data: {
        email: this.email,
        password: this.password,
      },
      xhrFields: {
        withCredentials: true,
      },
      crossDomain: true,
    })
    .then((response) => {
      // if the user has set his password before, auth him and redirect to leads page directly
      if(response.data.user.details.password_set){
        const authToken = response.data.auth_token;
        cookies.set('auth_token', authToken);
        cookies.set('name', response.data.user.details.name);
        cookies.set('email', response.data.user.details.email);
        cookies.set('userId', response.data.user.details.id);
        cookies.set('userRoles', response.data.user.roles);

        this.setState({isLoggedIn: true, waitingForLogging: false})
        this.props.history.push('/leads');

      } else {
        // if the user didnt set his password, open an input for him to set a new one
        const authToken = response.data.auth_token;
        cookies.set('auth_token', authToken);
        cookies.set('name', response.data.user.details.name);
        cookies.set('email', response.data.user.details.email);
        cookies.set('userId', response.data.user.details.id);
        cookies.set('userRoles', response.data.user.roles);
        this.setState({firstTime: true, userId: response.data.user.details.id})
      }

    }, (error) => {
      this.setState({isLoggedIn: false, waitingForLogging: false})
    });
  }

  setNewPassword(){
    axios({
      method: 'put',
      url: API.BASE + `/api/users/${this.state.userId}`,
      data: {
        email: this.email,
        password: this.state.newPassword,
        password_set: true
      },
      headers: { Authorization: cookies.get('auth_token') },
      xhrFields: {
        withCredentials: true,
      },
      crossDomain: true,
    }).then((response) => {
      this.props.history.push('/leads');
      this.setState({isLoggedIn: true, waitingForLogging: false})
    })
  }
  assignNewPasswordToState(event){
    this.setState({newPassword: event.target.value});
  }

  render() {
    return (
      <div className="conatiner-fluid login-page">
        <div className="caption">

          <div className={"col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4"}>
            <center className="login-text">
              <div>Login</div>
            </center>
            {(this.state.isLoggedIn == true) ?
              <p></p> :
              <div className="alert alert-danger">Wrong email or password </div>
            }
            <form className="form" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input className="form-control" type="email" name="email" onChange={this.setEmail} placeholder="Email" required/>
              </div>
              <div className="form-group">
                <input className="form-control" type="password" name="password" onChange={this.setPassword} placeholder="Password" required/>
              </div>
              {(this.state.firstTime)?
                <div className="form-group">
                  <input className="form-control" type="password" onChange={this.assignNewPasswordToState} placeholder="New password" required/>
                </div> : <p></p>
              }
              <div className="form-group">
                <center>

                  {(this.state.firstTime) ?
                    <button className="btn btn-primary btn-md btn-block" onClick={this.setNewPassword} >Set new password</button> :
                    (this.state.waitingForLogging)?
                    <input className="btn btn-primary btn-md btn-block" disabled type="submit" value="Logging in" />
                    : <input className="btn btn-primary btn-md btn-block" type="submit" value="Login" />
                  }
                </center>
              </div>
            </form>
          </div>

        </div>
      </div>
    );
  }
}

export default Login;
