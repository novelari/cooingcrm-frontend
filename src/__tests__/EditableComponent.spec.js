import React from 'react';
import { renderer } from 'react-test-renderer';
import { shallow, mount } from 'enzyme';
import EditableText from '../components/EditableText/index';

describe('Editable component', () => {


    it('should disply error message when input is not valid', () => {

        const component = shallow(<EditableText
                                    field={'budget'}
                                    validate={budget => budget > 0 || budget == '' ? '' : 'Invalid number'}
                                    value={-200}
                                    onSave={()=>{}}
                                />);
        component.instance().toEditMode();
                                
        component.instance().saveChanges();
        component.instance().renderEdit()
        component.update();
       
        expect (component.find('#error-validation').exists()).toEqual(true)
        expect (component.find('#error-validation').text()).toEqual('Invalid number')

    })


})