import React, { Component } from 'react';
import DateTime from 'react-datetime';
import moment from 'moment';
import axios from 'axios';
import Cookies from 'universal-cookie';
import API from '../../api/index';
import '../../styles/react-datetime.css'

const cookies = new Cookies();

class Alerts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment(),
      schedules: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.getAllSchedules = this.getAllSchedules.bind(this);
    this.createAlert = this.createAlert.bind(this);
    this.setDescription = this.setDescription.bind(this);
    this.deleteAlert = this.deleteAlert.bind(this);
    this.handleNotifyChange = this.handleNotifyChange.bind(this);
  }

  componentDidMount(){
    this.getAllSchedules();
  }

  handleChange(date) {
    this.setState({
      date: date,
    });

  }
  handleNotifyChange(e){
    this.setState({notifyAssignedLead:e.target.checked})

  }

  setDescription(e) {
    this.setState({description: e.target.value})
  }

  dateFromNow(date){
    var time = moment(date);
    return time.fromNow();
  }


  createAlert() {
    axios({
      method: 'POST',
      url: API.BASE +'/api/schedules',
      data: {
        date: this.state.date,
        lead_id: this.props.leadId,
        user_id: cookies.get('userId'),
        desc: this.state.description,
        notify_assigned_lead: this.state.notifyAssignedLead

      },
      headers: { Authorization: cookies.get('auth_token') },
    }).then((response) => {
      this.getAllSchedules();
    });
  }

  getAllSchedules() {
    axios({
      method: 'get',
      url: `${API.BASE}/api/${this.props.leadId}/schedules`,
      headers: { Authorization: cookies.get('auth_token') },
    })
    .then((response) => {
      this.setState({ schedules: response.data, isShowAlerts: true });
    },(error) => {console.log(error)});
  }

  deleteAlert(id){
    axios({
      method: 'delete',
      url: API.BASE+'/api/schedules/'+id,
      headers: { Authorization: cookies.get('auth_token') },
    })
    .then((response) => {
      const schedule = this.state.schedules.filter((schedule) => schedule.id == id)[0];
      this.setState((prevState) => prevState.schedules.splice(prevState.schedules.indexOf(schedule),1));
    });
  }


  validDate(current) {
    var now = new Date();
    let yesterday = moment().subtract(1,'day');
    return current.isAfter( yesterday );
  }

  userCanNotifyAssignedLead(){
     let userRoles = cookies.get('userRoles').map((role)=>role.role);
    if (userRoles.indexOf('sales_manager')>=0 || userRoles.indexOf('data_entry') >= 0){
      return true;
    }
    return false
  }


  render() {
    return (
      <div id="alerts-container">
        <h3>Alerts</h3>
        <div className="col-xs-12 nopadding-horizontal" id="alert-add">
          <div className="col-md-6 nopadding">
            <input className="form-control" placeholder="Description" onChange={this.setDescription}/>
          </div>
          <div className="col-md-2 nopadding">
            <DateTime
              closeOnSelect={true}
              value={this.state.date}
              onChange={this.handleChange}
              isValidDate={this.validDate}
            />
          </div>
          <div className="col-md-4">
            <button onClick={this.createAlert} className="btn btn-primary">New Alert</button>
          { this.userCanNotifyAssignedLead()?
              <div className="custom-checkbox">
                  <input type="checkbox" defaultChecked={true} name="notify-assigned" id="notify-assigned" onChange={this.handleNotifyChange} />
                  <label htmlFor="notify-assigned">Notify assigned lead</label>
              </div> : ''
          }
          </div>

        </div>
        {(this.state.schedules.length <=0)?
        <h4>No Alerts set for this lead</h4> :
        
        (<table className="table table-striped" id="alerts-table">
          <thead>
            <tr>
              <th>description</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {this.state.schedules.filter((alert)=> moment(alert.date).isAfter(moment())

            ).map(d => (
              <tr key={d.id}>
                <td>{d.desc}</td>
                <td>{this.dateFromNow(d.date)}</td>
                <td>
                  {d.user_id == cookies.get('userId')?
                    <button className="btn btn-danger" onClick={(id) => this.deleteAlert(d.id)}>
                      <span className="fa fa-times"></span>
                    </button> : ''
                  }
                </td>
              </tr>
            ))}
          </tbody>

        </table>)}
      </div>
    );
  }
}

export default Alerts;
