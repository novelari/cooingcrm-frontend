import React from 'react';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import { Creatable }from 'react-select';

const cookies = new Cookies();

export class SelectWithAdd extends React.Component {

    constructor(props) {
        super(props);
    }

    newOptionCreator(option){
      //get the id(value) from the last member of options (most recently created)
      let newOption = {label:option.label, value: option.label}
      
    
    return newOption;
  }

    isOptionUnique(option){
    // no duplicates case insensetive
    let value = option.option.label.toLowerCase();
    let values = option.options.map((option)=>option.label.toLowerCase());
    if (values.indexOf(value) >= 0){
      return false
    }
    return true
  }

  
  
    render() {
        return <Creatable
            value={this.props.value}
            options={this.props.options}
            onChange={this.props.onChange}
            searchable={true}
            clearable={false}
            newOptionCreator={this.newOptionCreator}
            isOptionUnique={this.isOptionUnique}
            onNewOptionClick={this.props.createNewOption}
        />

    }


}


export default SelectWithAdd;







