
const LeadStatus = {
  "Needs to be contacted": "success" ,
  "Following up": "info",
  "No Answer": "warning",
  "Stopped Answering": "danger",
  "Wrong Number": "default",
  "Not Interested":"warning" ,
  "Low Budget":"info",
  "Bought from someone else": "danger",
}

export default LeadStatus
