import axios from 'axios';
import Cookies from 'universal-cookie';
import API from '../api/index';

const cookies = new Cookies();

export function getLeads(){
     return axios({
        method: 'GET',
        url: API.BASE + '/api/leads',
        headers: { Authorization: cookies.get('auth_token') },
        })
    
}

export function deleteLead(id){
    return axios({
        method: 'delete',
        url: API.BASE + '/api/leads/' + id,
        headers: { Authorization: cookies.get('auth_token') },
      })
}

export function getPropertyTypes() {
    return axios({
        method: "GET",
        url: API.BASE + "/api/property_types",
        headers: { Authorization: cookies.get("auth_token") }
    })
}

export function addPropertyType(propertyType){
    return axios({
        method: "POST",
        data: {property_type: propertyType},
        url: API.BASE + "/api/property_types",
        headers: { Authorization: cookies.get("auth_token") }
    })
}

export function getStatuses(propertyType){
    return axios({
      method: "GET",
      url: API.BASE + "/api/statuses",
      headers: { Authorization: cookies.get("auth_token") }
    })
}

export function addStatus(status){
    return axios({
        method: "POST",
        data: {status: status},
        url: API.BASE + "/api/statuses",
        headers: { Authorization: cookies.get("auth_token") }
    })
}

export function getAgents(){
    return axios({
      method: 'GET',
      url: API.BASE + '/api/users/getAgents',
      headers: { Authorization: cookies.get('auth_token') }
    })
}