const LeadFields = [
    'id',
    'name',
    'telephone',
    'buyer/seller', 
    'date_of_first_contact',
    'method_of_contact',
    'best_time_to_call',
    'budget',
    'status',
    'date_of_trans',
    'what_was_bought',
    'unit_value',
    'location',
    'date_of_last_contact',
    'property_type'
]

export default LeadFields;