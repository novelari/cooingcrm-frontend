import React from 'react';

export class App404 extends React.Component {
  render() {
    return (
      <div className="container full-height app-404">
        <h3>404 page not found</h3>
        <p>We are sorry but the page you are looking for does not exist.</p>
      </div>
    );
  }
}

export default App404;
