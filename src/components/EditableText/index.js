import React from 'react';
import PropTypes from 'prop-types';
import { EditableComponent } from '../EditableComponent/index';

export class EditableText extends EditableComponent {
  renderEdit() {
    return (
      <div className={"edit"}>
        <input className={`form-control ${this.state.validationError?'error-input':''}`}
          onKeyDown={this.handleKeyDown}
          value={this.state.value}
          onChange={this.handleValueChange}
        />
          <i onClick={() => this.cancelChanges()} className="fa fa-times fa-lg" aria-hidden="true"></i>
          <i onClick={() => this.saveChanges()} className="fa fa-check fa-lg" aria-hidden="true"></i>
        {(this.state.validationError)? <small id="error-validation">{this.state.validationError}</small> : <p></p>}
      </div>
    );
  }

}

export default EditableText;
