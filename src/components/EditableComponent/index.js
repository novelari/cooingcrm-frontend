import React from 'react';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export class EditableComponent extends React.Component {

  constructor(props) {
    super(props);
    // TODO: replace flied with name and add type checking {{String}} 
    this.state = {
      editing: false,
      validationError: '',
      source: '', 
      value: '' , 
    };

    this.handleKeyDown      = this.handleKeyDown.bind(this);
    this.handleValueChange  = this.handleValueChange.bind(this);
    this.toEditMode         = this.toEditMode.bind(this);
  }

  render() {
    if(this.userCanEdit()){
      return this.state.editing ? this.renderEdit() : this.renderView();
    }
      return this.renderView();
    
  }

  userCanEdit() {
    //allow sales agents to edit any field except assignment
    if(this.props.field == 'assign-to'){
      let userRoles = cookies.get('userRoles').map((role) => role.role);
      if (userRoles.indexOf('sales_manager') >= 0 || userRoles.indexOf('data_entry') >= 0) {
        return true;
      }
      return false
    } else {
      return true;
    }
  }

  renderView() {
    return (
      <div onClick={this.toEditMode} className={"editable view"}>
        <span>
          <span className={this.props.customClass}>{this.props.value}</span>
        </span>
        {(cookies.get('userRoles') && this.userCanEdit())?
          (
            <span className={"icon"}>
              <i className="fa fa-pencil" aria-hidden="true" />
            </span>
          ):
          <p></p>
          
        }
        
      </div>
    );
  }

  handleKeyDown({ key }) {
    if (key === 'Escape') {
      this.cancelChanges();
    } else if (key === 'Enter') {
      this.saveChanges();
    }
  }

  cancelChanges() {
    this.setState({
      value: this.state.source,
      editing: false,
    });
  }

  saveChanges() {
    //if there is validation errors
    if (this.props.validate(this.state.value)) {
      this.setState({
        validationError:this.props.validate(this.state.value)
      });

      return;
    }
    
    this.setState({
      editing: false,
      validationError:''
    });
    this.props.onSave(this.props.field, this.state.value);
  }

  toEditMode(state = {}) { // TODO: merge object state with setState object
    // TODO: validate if we need to merge object state
    this.setState({
      // ...state,
      value:this.props.value,
      options: this.props.options,
      editing: true,
    });
  }

  toViewMode(state = {}) { // TODO: merge object state with setState object
    // TODO: validate if we need this method
    this.setState({
      // ...state,
      editing: false,
    });
  }

  handleValueChange(evt) {
    this.setState({ value: evt.target.value });
  }

  renderLabel() {
    // TODO: renderLabel func | label string
  }

}

EditableComponent.defaultProps = {
  onChange() {
    return;
  }
};

EditableComponent.propTypes = {
  onSave: PropTypes.func.isRequired,
  // value: PropTypes.any.isRequired, // TODO: require the value key
  validate: PropTypes.func.isRequired,
};

export default EditableComponent;
