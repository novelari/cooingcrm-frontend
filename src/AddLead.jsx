import React from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
import API from './api/index';
import { getPropertyTypes, addPropertyType, getStatuses, addStatus } from './api/asyncActions.js'
import Select, { Creatable } from 'react-select';
import SelectWithAdd from './components/SelectWithAdd';
import 'react-select/dist/react-select.css';

const cookies = new Cookies();

// import PropTypes from "prop-types";
var removeToast;

class AddLead extends React.Component {
  constructor(...props) {
    super(...props);
    this.state = {
      assignTo: '',
      statuses: [],
      propertyTypes: [],
      successAdd: false,
      addingLead: false,
      users: [],
      usersEmails: [],
      lead:{
        budget_currency: 'EGP',
        unit_value_currency:'EGP',
        validationErrorMessages: []
      }
    };
    // this.state.lead = { user_id: '1',budget_currency: 'EGP', unit_value_currency:'EGP', status:'Needs to be contacted' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.gotToLeads = this.gotToLeads.bind(this);
    this.handleChangeSelect = this.handleChangeSelect.bind(this)
    this.createNewPropertyType = this.createNewPropertyType.bind(this);
    this.createNewStatus = this.createNewStatus.bind(this);
   
  }

  componentDidMount(){
    this.getStatuses();
    this.getAgents();
    this.getPropertyTypes();
  }


  handleChange(event) {
    this.state.lead[event.target.name] = event.target.value;
    if (event.target.type === 'checkbox') {
      this.state.lead[event.target.name] = !!event.target.checked;
    }
    this.setState({lead: this.state.lead})
  }

  handleChangeSelect(obj, name) {
    if (name == 'user_id') {
      this.setState({ assignTo: obj.label })
    }

    this.state.lead[name] = obj.value;
    this.setState({lead: this.state.lead})
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      addingLead: true
    })
    axios({
      method: 'POST',
      url: `${API.BASE}/api/leads`,
      dataType: 'json',
      data: {lead: this.state.lead, manager_id: cookies.get('userId')},
      headers: { Authorization: cookies.get('auth_token') },
    })
      .then((response) => {
        this.setState({successAdd: true, validationErrorMessages: '', addingLead: false,
        lead: {
          budget_currency: 'EGP',
          unit_value_currency:'EGP',
          status:'Needs to be contacted',
          validationErrorMessages: []
        }},
            () => {
              removeToast = window.setTimeout(() => {this.setState({successAdd:false})}, 5000);
              return removeToast;
              
          })
        this.refs['new-lead-form'].reset();
       }, (error) => {
        //  if error from form validation
        if (error.response.status == 422) {
          const errors = error.response.data;
          let errorMessages = []
          for (let key in errors) {
            errorMessages.push(`${key} : ${errors[key]}`)
          }
          this.setState({ validationErrorMessages: errorMessages, addingLead: false })
        }
        else {
        }
        //
      });
  }

  gotToLeads(){
    this.props.history.push('/leads');
  }

  componentWillUnmount(){
    window.clearTimeout(removeToast)
  }
  
  getStatuses() {
    getStatuses().then(response => {
      var statuses = []
      for (let option of response.data) {
        var status = { value: option.id, label: option.status };
        statuses.push(status);
      }
      this.setState({ statuses: statuses });
    });
  }
  
  getPropertyTypes(){

    getPropertyTypes().then(response => {
      var propertyTypes = []
      for (let type of response.data) {
        var propertyType = { value: type.property_type, label: type.property_type };
        propertyTypes.push(propertyType);
      }
      this.setState({ propertyTypes: propertyTypes });
    })

  }

  
  getAgents(event) {
   
    !this.state.users.length &&
    axios({
      method: 'GET',
      url: API.BASE + '/api/users/getAgents',
      headers: { Authorization: cookies.get('auth_token') }
    }).then(response => {
      let usersEmails = [];
      for(let user of response.data){
        usersEmails.push({value:user.id, label:user.email});
      }
      this.setState({ users: response.data,usersEmails: usersEmails });
    });
  }

  createNewPropertyType(option){
    addPropertyType(option.value).then(response=>{
      let propertyType = {value: response.data.property_type, label: response.data.property_type}
      this.setState((prevState)=>{prevState.propertyTypes.push(propertyType)})
    })
  }

  createNewStatus(option){
    //we need this when adding new status to get the status id
    let promise = Promise.resolve(addStatus(option.value));
    promise.then(response=>{
      let status = {value: response.data.id, label: response.data.status}
      this.setState((prevState)=>{prevState.statuses.push(status)})
    })
    return promise;
  }
  
  

  render() {
    return (
      <div className="add-lead container">
       
        {(this.state.validationErrorMessages)?
         (this.state.validationErrorMessages.map((message, index) => <div key={index} className="alert alert-danger">{message}</div>) ): <p></p>
        }

        <form className="form form-inline" onSubmit={this.handleSubmit} id="add-new-lead-form" ref="new-lead-form">
            {(this.state.successAdd == true) ?
              <div className="alert alert-success add-lead-success">Lead Added Successfully</div> : <div style={{height:"52px"}}></div>
            }
            <div className="row-collapse">
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="name">Name:</label>
                <input className="form-control" type="text" name="name" onChange={this.handleChange} onBlur={this.handleChange} required/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <div className="custom-checkbox">
                  <input type="checkbox" name="buyer" id="buyer" onChange={this.handleChange} onBlur={this.handleChange}/>
                  <label htmlFor="buyer">Buyer:</label>
                </div>
                <div className="custom-checkbox">

                  <input type="checkbox" name="seller" id="seller" onChange={this.handleChange} onBlur={this.handleChange}/>
                  <label htmlFor="seller">Seller:</label>
                </div>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6" >
                <label htmlFor="status">Status:</label>
                <SelectWithAdd 
                  value={(this.state.statuses.length>0 && this.state.lead.status_id) &&this.state.statuses.filter((status)=>status.value == this.state.lead.status_id)[0]}
                  options={this.state.statuses}
                  onChange={(e)=>this.handleChangeSelect(e,'status_id')}
                  createNewOption={this.createNewStatus}
                />
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6" >
                <label htmlFor="telephone">Telephone:</label>
                <input className="form-control" type="text" name="telephone" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="email">Email:</label>
                <input className="form-control" type="email" name="email" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="date_of_first_contact">Date of first contact:</label>
                <input className="form-control" type="date" name="date_of_first_contact" onChange={this.handleChange} required/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="method_of_contact">Method of contact:</label>
                <input className="form-control" type="text" name="method_of_contact" onChange={this.handleChange} onBlur={this.handleChange} required/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="best_time_to_call">Best time to call:</label>
                <input className="form-control" type="text" name="best_time_to_call" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="budget">Budget:</label>
                <input className="form-control" type="number" name="budget" onChange={this.handleChange} placeholder="Budget"/>
                <div className="input-group-btn">
                  <select className="form-control" name="budget_currency" onChange={this.handleChange}>
                    <option value="EGP">EGP</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>

                </div>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="date_of_trans">Date of transaction:</label>
                <input className="form-control" type="date" name="date_of_trans" onChange={this.handleChange} />
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="unit-value">Unit Value:</label>
                <input className="form-control" type="number" name="unit_value" onChange={this.handleChange} placeholder="Unit value"/>
                <div className="input-group-btn">
                  <select className="form-control" name="unit_value_currency" onChange={this.handleChange}>
                    <option value="EGP">EGP</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>

                </div>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="what_was_bought">What was bought:</label>
                <input className="form-control" type="text" name="what_was_bought" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6" >
                <label htmlFor="property_type">Property type:</label>
                <SelectWithAdd 
                  value={this.state.lead.property_type}
                  options={this.state.propertyTypes}
                  onChange={(e)=>this.handleChangeSelect(e,'property_type')}
                  createNewOption={this.createNewPropertyType}
                  />
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="location">Location:</label>
                <input className="form-control" type="text" name="location" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
              
              <div className="input-group col-xs-12 col-md-6 col-lg-6">
                <label htmlFor="date_of_last_contact">Date of last contact:</label>
                <input className="form-control" type="date" name="date_of_last_contact" onChange={this.handleChange} />
              </div>
              <div className="input-group col-xs-12 col-md-6 col-lg-6" >
                <label htmlFor="user_id">Assign user:</label>
                <Select
                  name="user_id"
                  value={this.state.lead.user_id}
                  options={this.state.usersEmails}
                  onChange={(value)=>this.handleChangeSelect(value, 'user_id')}
                  searchable={true}
                  clearable={false}
                  />
              </div>
            </div>
            
            <div className="row-collapse">
              <div className="input-group col-xs-12">
                <label htmlFor="desc">Description:</label>
                <textarea className="form-control" type="text" name="desc" onChange={this.handleChange} onBlur={this.handleChange}/>
              </div>
            </div>

            {this.state.addingLead? 
              <div className="pull-right add-cancel-buttons">
                <input disabled className="btn btn-primary" type="submit" value="Adding Lead" />
                <button disabled id="cancel-btn" className="btn btn-danger" onClick={this.gotToLeads}>Cancel</button>
              </div> :

              <div className="pull-right add-cancel-buttons">
                <input className="btn btn-primary" type="submit" value="Add Lead" />
                <button id="cancel-btn" className="btn btn-danger" onClick={this.gotToLeads}>Cancel</button>
              </div>
            }
        </form>
      </div>
    );
  }
}

export default AddLead;
