import React from 'react';
import moment from 'moment';

export class Comment extends React.Component {
  render() {
      return (
        <div id="comment-container">
          <strong id="comment-user">{this.props.comment.commenter_name || this.props.comment.commenter_email}</strong>
          <small id="comment-date">{moment(this.props.comment.created_at).fromNow()}</small>
          {this.props.currentUserId == this.props.comment.commenter_id?
            <a id="comment-delete" onClick={this.props.onDelete} href="#">
              <i className="fa fa-trash"></i>
            </a> : ''
          }
          <p>{this.props.comment.message}</p>
          
        </div>
      )
  }
}

export default Comment;
