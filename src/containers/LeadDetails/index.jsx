import React from 'react';
import axios from 'axios';
import API from '../../api/index';

import Cookies from 'universal-cookie';

import { LeftSideView } from './LeftSideView';
import Navbar from '../../components/navbar/index';
import fakeLead from './FakeLead';

import Alerts from './Alerts';
import RightSideView from './RightSideView';

import EditableText from '../../components/EditableText/index';
import EditableTextarea from '../../components/EditableTextarea/index';
import EditableDropdown from '../../components/EditableDropdown/index.js';
import EditableDate from '../../components/EditableDate/index.js'
import EditableCreatable from '../../components/EditableCreatable/index.js'

import Comments from '../Comments/Comments'
import { getStatuses, getPropertyTypes, addStatus, addPropertyType } from '../../api/asyncActions.js'


const cookies = new Cookies();

/*

Manual leads are leads we get when someone directly calls our phoneline.ii.
The fields we need for each customer are:
Client Name, Sales Agent,Date of 1st Contact, Method of Contact, Telephone #, Email Address

Status (Needs Follow Up, Not Interested, waiting for new launch, meetingset up, etc),
Comments, Buyer/Seller, Best time to call, Short Description,Budget, Date of Transaction (if he/she buys),
what was bought, UnitValue, Commission Earnediii.
Mandatory Fields: Client name, date of 1st contact, method of contact,telephone #, status
*/

/*
[
Client Name, Sales Agent, Date of 1st Contact,
Method of contact, Telephone, Email, Status,

Comments, Buyer/Seller, Best time to call, Short Description, Budget,
Date of Transaction, what was bought, UnitValue, Commission Earned,
]
*/

export class LeadDetails extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      statuses: [],
      propertyTypes: [],
      leadData: {},
      confirmDelete: false
    };

    this.updateLead = this.updateLead.bind(this);
    this.deleteLead = this.deleteLead.bind(this);
    this.archiveLead = this.archiveLead.bind(this);
    this.unArchiveLead = this.unArchiveLead.bind(this);
    this.openConfirmDelete = this.openConfirmDelete.bind(this);
    this.closeConfirmDelete = this.closeConfirmDelete.bind(this);
    this.createNewStatus = this.createNewStatus.bind(this);
    this.createNewPropertyType = this.createNewPropertyType.bind(this);
  }

  componentDidMount() {
    this.getStatuses();
    this.getPropertyTypes();
    axios({
      method: 'GET',
      url: API.BASE + '/api/leads/' + this.props.match.params.id,
      headers: { Authorization: cookies.get('auth_token') }
    }).then(response => {
      let lead = response.data.lead
      lead.assignTo = response.data.user_email
      this.setState({ leadData: response.data.lead });
    });
  }

  updateLead(field, value) {
    let newLead;
    if (field == "buyer_seller") {
      if (value == 'Seller') {
        newLead = Object.assign({}, this.state.leadData, { 'seller': true, 'buyer': false });
      } else {
        newLead = Object.assign({}, this.state.leadData, { 'seller': false, 'buyer': true });
      } 
   
    } else {
      newLead = Object.assign({}, this.state.leadData, { [field]: value });

    }
    this.setState({ leadData: newLead });

    axios({
      method: 'PUT',
      url: API.BASE + '/api/leads/' + this.props.match.params.id,
      data: newLead,
      headers: { Authorization: cookies.get('auth_token') }
    }).then(response => {
      // TODO this should be changed to a more robust solution, its necessary because when 
      // updating any field we lose the assignTo property
      response.data.assignTo = this.state.leadData.assignTo;
      this.setState({ leadData: response.data });
    });
  }

  openConfirmDelete() {
    this.setState({ confirmDelete: true })
  }
  closeConfirmDelete() {
    this.setState({ confirmDelete: false })
  }
  //permanent delete
  deleteLead() {
    axios({
      method: 'DELETE',
      url: API.BASE + '/api/leads/' + this.props.match.params.id,
      headers: { Authorization: cookies.get('auth_token') },
    }).then((response) => {
      this.props.history.push('/leads');
    });
  }

  archiveLead() {
    axios({
      method: 'PUT',
      url: API.BASE + '/api/leads/' + this.props.match.params.id + '/archive',
      headers: { Authorization: cookies.get('auth_token') },
    }).then((response) => {
      this.props.history.push('/leads');
    });
  }

  unArchiveLead() {
    axios({
      method: 'PUT',
      url: API.BASE + '/api/leads/' + this.props.match.params.id + '/un_archive',
      headers: { Authorization: cookies.get('auth_token') },
    }).then((response) => {
      this.setState({leadData: response.data})
    });
  }

  getStatuses() {
    getStatuses().then(response => {
      var statuses = []
      for (let option of response.data) {
        var status = {label:option.status, value: option.id};
        statuses.push(status);
      }
      this.setState({ statuses: statuses });
    });
  }
  validateFutureDate(date) {
    if (new Date(date) < Date.now()) {
      return "Cannot be in the past";
    }
  }

  getPropertyTypes() {
   getPropertyTypes().then(response => {
      var propertyTypes = []
      for (let type of response.data) {
        propertyTypes.push({label: type.property_type, value: type.property_type});
      }
      this.setState({ propertyTypes: propertyTypes });
    });
  }

  createNewStatus(option){
    
    let promise = Promise.resolve(addStatus(option.value))
    promise.then(response=>{
      let status = {value: response.data.id, label: response.data.status}
      this.setState((prevState)=>{prevState.statuses.push(status)})
      
    })
    
    return promise;
  }

  createNewPropertyType(option){
    addPropertyType(option.value).then(response=>{
      let propertyType = {value: response.data.property_type, label: response.data.property_type}
      this.setState((prevState)=>{prevState.propertyTypes.push(propertyType)})
    })
  }

  usercanDeleteLead() {
    let userRoles = cookies.get('userRoles').map((role) => role.role);
    if (userRoles.indexOf('data_entry') >= 0 || userRoles.indexOf('sales_manager')) {
      return true;
    }
    return false
  }


  render() {
    return (
      <div className={'lead-details container'}>
        <div className={'lead-top-bar'}>
          <div className={'text-right'}>
            {/*<button type="button" className="btn btn-primary" onClick={() => 'this.printLead'}>
              <i className="fa fa-print" aria-hidden="true" />
            </button>*/}
            {/*{this.state.leadData.archived ?
              <button className="btn btn-primary" onClick={this.unArchiveLead}>Retrieve lead</button> :

              ((!this.state.confirmDelete) ?
                (<button type="button" className="btn btn-danger" onClick={this.openConfirmDelete}>
                  <i className="fa fa-trash" aria-hidden="true" />
                </button>) :
                (<div>
                  <span>Are you sure you want to delete this lead</span>
                  <br />
                  <button className="btn btn-danger" onClick={this.archiveLead}>Delete</button>
                  <button className="btn btn-success" onClick={this.closeConfirmDelete}>No</button>
                </div>))
            }*/}
            {!this.usercanDeleteLead()? 

             (this.state.leadData.archived ?
              <button className="btn btn-primary" onClick={this.unArchiveLead}>Retrieve lead</button> :
              <button className="btn btn-danger" onClick={this.archiveLead}>Archive &nbsp;
                <i className="fa fa-trash" aria-hidden="true" />
              </button>
             ) : ''
            }
          </div>
        </div>
        <h2 id="lead-details-header">{this.state.leadData.id} - {this.state.leadData.name}</h2>

        <LeftSideView onSave={this.updateLead} leadData={this.state.leadData} />

        <div className={'col-xs-12 col-md-6 lead-container'}>

          <div className={'lead-field'} id='budget-editable'>
            <label>Budget: </label>
            <EditableText
              field={'budget'}
              validate={budget => budget > 0 || budget == '' ? '' : 'Invalid number'}
              value={this.state.leadData['budget'] || "Not set"}
              onSave={this.updateLead}
            />
            <EditableDropdown
              field='budget_currency'
              value={this.state.leadData['budget_currency']}
              validate={() => false}
              options={["EGP", "USD", "EUR"]}
              onSave={this.updateLead}
            />
          </div>

          <div className={'lead-field'} id='unit-value-editable'>
            <label>Unit value: </label>
            <EditableText
              field={'unit_value'}
              validate={value => value > 0 || value == '' ? '' : 'Invalid number'}
              value={this.state.leadData['unit_value'] || "Not set"}
              onSave={this.updateLead}
            />
            <EditableDropdown
              field='unit_value_currency'
              value={this.state.leadData['unit_value_currency']}
              validate={() => false}
              options={["EGP", "USD", "EUR"]}
              onSave={this.updateLead}
            />
          </div>

          <div className={'lead-field'}>
            <label>Location: </label>
            <EditableText
              field={'location'}
              validate={() => false}
              value={this.state.leadData['location'] || "Not set"}
              onSave={this.updateLead}
            />
          </div>

          <div className={'lead-field'}>
            <label>What was bought: </label>
            <EditableText
              field={'what_was_bought'}
              validate={() => false}
              value={this.state.leadData['what_was_bought'] || "None"}
              onSave={this.updateLead}
            />
          </div>

          <div className={"lead-field"}>
            <label> Date of transaction: </label>
            <EditableDate
              field={"date_of_trans"}
              value={this.state.leadData['date_of_trans'] || 'Not set'}
              validate={this.validateFutureDate}
              onSave={this.updateLead}
              onChange={() => { }}

            />
          </div>

          <div className={"lead-field"}>
            <label style={{ marginBottom: "5px" }}> Property type: </label>
            <EditableCreatable
              field={"property_type"}
              value={this.state.leadData.property_type || 'Not set'}
              validate={() => false}
              options={this.state.propertyTypes}
              onSave={this.updateLead}
              onCreateNewOption={this.createNewPropertyType}
            />
          </div>

          <div className={'lead-field'}>
            <label>Short Description: </label>
            <EditableTextarea
              field={'desc'}
              validate={() => false}
              value={this.state.leadData['desc'] || "No description"}
              onSave={this.updateLead}
            />
          </div>

          

        </div>

        <RightSideView 
            leadData={this.state.leadData} 
            onSave={this.updateLead} 
            statuses={this.state.statuses}
            createNewStatus={this.createNewStatus} />

        <Alerts leadId={this.props.match.params.id} />
        <Comments leadId={this.props.match.params.id} />



      </div>
    );
  }

}

export default LeadDetails;
