import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router';
import LeadStatus from '../../LeadStatus';


const currency = {
  "EGP": "egp-currency",
  "USD":"fa fa-dollar",
  "EUR":"fa fa-euro"
}

class Lead extends React.Component {
  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this);
    this.state = {redirect: false}
  }
  handleClick(){
    this.setState({redirect:true})
  }
  isVisible(field){
    if(this.props.columnsToShow.indexOf(field) >= 0){
      return "table-cell";
    } else {
      return "none";
    }
  }
  render(){
    // <tr id="lead-component">
    //   <td>
    //     <h4><strong>{this.props.name}</strong></h4>
    //   </td>
    //   <td>
    //     <i className="fa fa-envelope" />{this.props.email}
    //     <br />
    //     <i className="fa fa-phone" />{this.props.telephone}
    //   </td>
    //   <td>First contacted {moment(this.props.firstContact).fromNow()}</td>
    //   <td><span className={`alert alert-${LeadStatus[this.props.status]}`}>{this.props.status}</span></td>
    //   <td>
    //     <Link to={`leads/${this.props.id}`} className="btn btn-info"><span className="fa fa-id-card-o" /></Link>
    //     <button className="btn btn-danger" onClick={this.props.deleteLead}><span className="fa fa-trash" /></button>
    //   </td>
    // </tr>
     if (this.state.redirect) {
        return <Redirect push to={`/leads/${this.props.lead.id}`} />;
      }

    return (
    <tr onClick={this.handleClick} className={`lead-component ${this.props.lead.archived? "deleted":""}`}>
       <td style={{display:this.isVisible("id")}}>
        {this.props.lead.id}
      </td>
      <td style={{display:this.isVisible("name")}} id="name">
        {this.props.lead.name}
      </td>
      <td style={{display:this.isVisible("telephone")}}>
        {this.props.lead.telephone}
      </td>
      <td style={{display:this.isVisible("buyer/seller")}}>
        {(this.props.lead.buyer)?"Buyer" : (this.props.lead.seller)? "Seller" : "Not set"}
      </td>
      <td style={{display:this.isVisible("date_of_first_contact")}}>
        {this.props.lead.date_of_first_contact && moment(this.props.lead.date_of_first_contact).format('ll')}
      </td>
      <td style={{display:this.isVisible("method_of_contact")}}>
        {this.props.lead.method_of_contact}
      </td>
      <td style={{display:this.isVisible("best_time_to_call")}}>
        {this.props.lead.best_time_to_call}
      </td>
      <td style={{display:this.isVisible("budget")}}>
        {/*to add commas to the number*/}
        {(this.props.lead.budget)?
          <span>{this.props.lead.budget.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } &nbsp;
             <i className={`${currency[this.props.lead.budget_currency]}`} />
          </span>
            : "Not set"}
      </td>
      <td style={{display:this.isVisible("status")}}>
        <span className={`alert alert-${LeadStatus[this.props.lead.status_text] || "default"}`}>{this.props.lead.status_text}</span>
      </td>
      <td style={{display:this.isVisible("email")}}>
        {this.props.lead.email}
      </td>
       <td style={{display:this.isVisible("date_of_trans")}}>
        {this.props.lead.date_of_trans && moment(this.props.lead.date_of_trans).format('ll')}
      </td>
      <td style={{display:this.isVisible("what_was_bought")}}>
        {this.props.lead.what_was_bought}
      </td>
      <td style={{display:this.isVisible("unit_value")}}>
        {(this.props.lead.unit_value)?
          <span>{this.props.lead.unit_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") } &nbsp;
             <i className={`${currency[this.props.lead.unit_value_currency]}`} />
          </span>
            : "Not set"}
      </td>
      <td style={{display:this.isVisible("location")}}>
        {this.props.lead.location}
      </td>
      <td style={{display:this.isVisible("date_of_last_contact")}}>
        {this.props.lead.date_of_last_contact && moment(this.props.lead.date_of_last_contact).format('ll')}
      </td>
      <td style={{display:this.isVisible("property_type")}}>
        {this.props.lead.property_type}
      </td>
    </tr>
  )};
};

export default Lead;
