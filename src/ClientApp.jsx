import React from 'react';
import ReactDOM from 'react-dom';
// import 'jquery';
// import 'bootstrap';

import './styles/style.css';
import './styles/style.sass';
import Leads from './containers/Leads/Leads';
import Login from './Login';
import AddLead from './AddLead';
import App404 from './App404';
import App401 from './App401';
import LeadDetails from './containers/LeadDetails';
import Navbar from './components/navbar/index';
import 'font-awesome/css/font-awesome.css';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className={"routes-container"}>
          <Navbar logoutEnabled/>
          <Switch>
            <LoggedInRoute exact path="/" component={Login}/>
            <PrivateRoute exact path="/leads" component={Leads}/>
            <PrivateRoute path="/leads/new" component={AddLead}/>
            <PrivateRoute path="/leads/:id" component={LeadDetails}/>
            <Route path="/unauthorized" component={App401}/>
            <Route component={App404}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

const PrivateRoute = ({component: Component, path}) => (
  <Route path={path} render={props => (
    cookies.get('auth_token') ?
    (<Component {...props}/> ) :
    (
      <Redirect to={{
        pathname: '/unauthorized',
        state: { from: props.location }
      }} />
    )
  )} />
)

const LoggedInRoute = ({component: Component, path}) => (
  <Route path={path} render={props => (cookies.get('auth_token') ?
    (
      <Redirect to={{
        pathname: '/leads',
        state: { from: props.location }
      }} />
    ) : (<Component {...props}/> )
  )}/>
)


ReactDOM.render(
  <App />,
  document.getElementById('app'));
