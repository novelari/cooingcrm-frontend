import React, {
  Component,
} from 'react';
import EditableText from '../../components/EditableText/index';
import EditableDate from '../../components/EditableDate/index.js'

export class LeftSideView extends React.Component {
  constructor(props){
    super(props);
    this.validateEmail = this .validateEmail.bind(this);
    this.validateTelephone = this.validateTelephone.bind(this);
  }

  
  validateTelephone(tel){
    if(tel){
      if(tel.split('').length <= 4){
        return "Telephone number must be more than 4 numbers";
      } else {
        return '';
      }
    } else {
      if(!this.props.leadData.email){
        return 'Must enter email or telephone no.'
      }
    }
  }

  validateEmail(email){
    if(email){
      const validMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!validMail.test(email)){
        return "Not a valid email addres";
      } else {
        return '';
      }
    } else {
      if(!this.props.leadData.telephone){
        return 'Must enter email or telephone no.'
      }
    }
  }
  validatePresence(data){
    if(!data){
      return "Cannot be empty";
    } else {
      return '';
    }
  }
  validatePastDate(date){
    if(new Date(date) > Date.now()){
      return "Cannot be in the future";
    }
  }
  
  render() {
    return (
      <div className={"col-xs-12 col-md-3 no-padding lead-container-box"}>

        <div className={"lead-field"}>
          <label> Name: </label>
          <EditableText
            field={"name"}
            value={this.props.leadData.name}
            validate={this.validatePresence}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

        <div className={"lead-field"}>
          <label> Phone Number: </label>
          <EditableText
            field={"telephone"}
            value={this.props.leadData.telephone || 'Not set'}
            validate={this.validateTelephone}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

        <div className={"lead-field"}>
          <label> Email: </label>
          <EditableText
            field={"email"}
            value={this.props.leadData.email || 'Not set'}
            validate={this.validateEmail}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

        <div className={"lead-field"}>
          <label> Method of contact: </label>
          <EditableText
            field={"method_of_contact"}
            value={this.props.leadData.method_of_contact}
            validate={this.validatePresence}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

        <div className={"lead-field"}>
          <label> Best time to call: </label>
          <EditableText
            field={"best_time_to_call"}
            value={this.props.leadData.best_time_to_call}
            validate={this.validatePresence}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

        <div className={"lead-field"}>
          <label> Date of first contact: </label>
          <EditableDate
            field={"date_of_first_contact"}
            value={this.props.leadData.date_of_first_contact}
            validate={this.validatePastDate}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>
         <div className={"lead-field"}>
          <label> Date of last contact: </label>
          <EditableDate
            field={"date_of_last_contact"}
            value={this.props.leadData.date_of_last_contact || 'Not set'}
            validate={this.validatePastDate}
            onSave={this.props.onSave}
            onChange={()=>{}}
            save={this.props.save}
          />
        </div>

      </div>
    );
  }

}

export default LeftSideView;
