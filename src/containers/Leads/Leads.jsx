import React, { Component } from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';
import { Link } from 'react-router-dom';
import ReactPaginate from 'react-paginate';
import AddLead from '../../AddLead';
import Lead from './Lead';
import API from '../../api/index.js';
import Navbar from '../../components/navbar/index.js';
import LeadFields from '../../LeadFields.js';
import TableOptions from './TableOptions'
import { getLeads, deleteLead,getPropertyTypes, getStatuses, getAgents } from '../../api/asyncActions.js'

import moment from "moment";
import DateRangePicker from "react-bootstrap-daterangepicker";
import { StyleSheet } from "react";
import $ from "jquery";
import Select from 'react-select'


const cookies = new Cookies();

const DropDownLink = (props) =>
  <a onClick={() =>
    (props.clickHandler(props))}>{props.status}</a>

class Leads extends Component {
  constructor(...props) {
    super(...props);
    this.searchUrl = '';
    this.state = {
      allStatuses: [],
      leads: [],
      statusSelected: 'All',
      searchTerm: '',
      tableOptionsIsOpened: false,
      columnsToShow: [],
      allPropertyTypes: [{label:'Apartment',value:"Apartment"},{label:'Villa',value:'Villa'},{label:'Duplex', value:'duplex'}],
      selectedPropertyTypes: [],
      seller: false,
      buyer: false,
      dateOfFirstContact: " ",
      start_Date: '',
      end_date: '',
      start_DateOfTrans: '',
      end_DateofTrans: '',
      LstTimeFOF: ' ',
      EndTimeFOF: ' ',
      fstTimeReq: ' ',
      lstTimeReq: ' ',
      fstTimeReqtobcont: ' ',
      lstTimeReqtobcont: ' ',
      searchKey: [],
      budget_currency: 'EGP',
      unit_value_currency: 'EGP',
      budgetFrom: [],
      budgetTo: [],
      unit_vlueFrom: [],
      unit_valueTo: [],
      bestTimeOfCall: [],
      numberOfSearch: [],
      selectedStatuses: [],
      selectedPage: 0,
      perPage: 10,  //now it is static, but was put here in state in case we wanted to change it from within the app
      totalLeadCount: 0,
      orderLeadsBy: 'created_at',
      orderLeadsDirection: 'ASC',
      archivedIsShown: false
    };
    this.lead = { seller: false, buyer: false };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.toggleTableOptions = this.toggleTableOptions.bind(this);
    this.handleColumnSelection = this.handleColumnSelection.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.DatePic = this.DatePic.bind(this);
    this.DateOfTrans = this.DateOfTrans.bind(this);
    this.getLead = this.getLead.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
    this.orderLeads = this.orderLeads.bind(this);
    this.getAgents = this.getAgents.bind(this);
    this.toggleShowArchived = this.toggleShowArchived.bind(this);

  }
  DatePic(picker, event) {
    var that = this;
    var start = moment();
    var end = moment();

    function cb(start, end) {
      that.goToInitialPage();
      // $("#reportrange span").html(
      //   start.format("MMMM D, YYYY") + " - " + end.format("MMMM D, YYYY")
      // );

      that.setState({
        start_Date: start.format("YYYY-MM-DD"),
        end_date: end.format("YYYY-MM-DD")
      }, () => that.getLead()
      );
      that.getLead();
    }

    $("#reportrange").daterangepicker(
      {
        startDate: start,
        endDate: end,
        autoUpdateInput: false,
        locale: {
          cancelLabel: 'Clear'
        },
        ranges: {
          Yesterday: [
            moment().subtract(1, "days"),
            moment()
          ],
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()],
          "This Month": [moment().startOf("month"), moment().endOf("month")],
          "Last Month": [
            moment().subtract(1, "month").startOf("month"),
            moment().subtract(1, "month").endOf("month")
          ]
        }
      },
      cb
    );

    $("#reportrange").on('cancel.daterangepicker', function (ev, picker) {
      this.setState({
        start_Date: '',
        end_date: ''
      }, () => that.getLead()
      );
    }.bind(this))

    //cb(start, end);
  }
  // date picker for date of trans
  DateOfTrans(picker, event) {
    var that = this;
    var start = moment();
    var end = moment();

    function cb(start, end) {
      that.goToInitialPage();
      // $("#reportrangeDateOftrans span").html(
      //   start.format("MMMM D, YYYY") + " - " + end.format("MMMM D, YYYY")
      // );

      that.setState({
        start_DateOfTrans: start.format("YYYY-MM-DD"),
        end_DateofTrans: end.format("YYYY-MM-DD")
      }, () => that.getLead()
      );

    }

    $("#reportrangeDateOftrans").daterangepicker(
      {
        startDate: start,
        endDate: end,
        autoUpdateInput: false,
        locale: {
          cancelLabel: 'Clear'
        },
        ranges: {
          Yesterday: [
            moment().subtract(1, "days"),
            moment()
          ],

          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()],
          "This Month": [moment().startOf("month"), moment().endOf("month")],
          "Last Month": [
            moment().subtract(1, "month").startOf("month"),
            moment().subtract(1, "month").endOf("month")
          ]
        }
      },
      cb
    );

    $("#reportrangeDateOftrans").on('cancel.daterangepicker', function (ev, picker) {
      this.setState({
        start_DateOfTrans: '',
        end_DateofTrans: ''
      }, () => that.getLead()
      );
    }.bind(this))


  }

  componentDidMount() {
    this.DateOfTrans();
    this.DatePic();
    this.getStatuses();
    this.getPropertyTypes();
    this.getLead();
    this.getAgents();
    this.setState({
      columnsToShow: cookies.get('columns_to_show') && cookies.get('columns_to_show').split(',') ||
      ['id','name', 'telephone', 'buyer/seller', 'date_of_first_contact',
        'method_of_contact', 'best_time_to_call', 'budget', 'status']
    })

  }

  handleSelectChange(data, field) {
    this.goToInitialPage();
    var options = [];
    for (var key in data) {
      var obj = data[key];
      options.push(obj.value)

    }
    this.setState({ [field]: options }, () => {
      this.getLead();
    
  });

  }

  handleChange(event,name) {
    this.goToInitialPage();
    var that = this
    this.lead[event.target.name] = event.target.value;
    if (event.target.type === "checkbox") {
      this.lead[event.target.name] = (event.target.checked);
      this.setState({ seller: this.lead.seller,buyer: this.lead.buyer }, () => this.getLead())
    } else {
      this.setState({
        [event.target.name]: event.target.value
        // budgetFrom: this.lead.budgetFrom,
        // budgetTo: this.lead.budgetTo,
        // budget_currency: this.lead.budget_currency,
        // unit_vlueFrom: this.lead.unit_vlueFrom,
        // unit_valueTO: this.lead.unit_valueTO,
        // unit_value_currency: this.lead.unit_value_currency
      }, () => this.getLead());
    }
  }

 
  getLead() {
    this.searchUrl =
      this.state.search +
      "&status=" +
      this.state.selectedStatuses +
      "&property_type=" +
      this.state.selectedPropertyTypes + 
      "&seller=" +
      this.state.seller +
      "&buyer=" +
      this.state.buyer +
      "&startDateOfContact=" +
      this.state.start_Date +
      "&endDateOfContact=" +
      this.state.end_date +
      "&startDateOfTrans=" +
      this.state.start_DateOfTrans +
      "&endDateOfTrans=" +
      this.state.end_DateofTrans +
      "&budgetFrom=" +
      this.state.budgetFrom +
      "&budgetTo=" +
      this.state.budgetTo +
      "&budgetCurrency=" +
      this.state.budget_currency +
      "&unitvaluefrom=" +
      this.state.unit_vlueFrom +
      "&unitvalueto=" +
      this.state.unit_valueTO +
      "&unitvaluecurrency=" +
      this.state.unit_value_currency +
      "&bestTimeOfCall=" +
      this.state.bestTimeOfCall +
      "&assigned_users=" +
      this.state.selectedAssignedUsers +
      "&show_archived=" +
      this.state.archivedIsShown
      
    var paginationParams = 
      "&per_page=" + this.state.perPage + 
      "&page=" + (this.state.selectedPage+1) +
      "&order_by=" + this.state.orderLeadsBy +
      "&order_direction=" + this.state.orderLeadsDirection

    axios({
      method: "GET",
      url: API.BASE + "/api/leads?search=" + this.searchUrl + paginationParams,
      headers: { Authorization: cookies.get("auth_token") }
    }).then(response => {
      this.setState({ leads: response.data.leads, 
        numberOfSearch: Object.keys(response.data.leads).length,
        totalLeadCount:response.data.meta.total_count,
        perPage: response.data.meta.limit
         });
      cookies.set('search-url', this.searchUrl)
    });

  }
  
  
  toggleTableOptions() {
    if (this.state.tableOptionsIsOpened) {
      this.setState({ tableOptionsIsOpened: false })
    } else {
      this.setState({ tableOptionsIsOpened: true })
    }
  }
  handleColumnSelection(event) {
    var name = event.target.name;
    if (event.target.checked) {
      this.setState((prevState) => { prevState.columnsToShow.push(name) }, 
          ()=>{cookies.set('columns_to_show', this.state.columnsToShow.join(','))})
    } else {
      this.setState((prevState) => {
        prevState.columnsToShow.splice(
          prevState.columnsToShow.indexOf(name), 1
        )
      },()=>{cookies.set('columns_to_show', this.state.columnsToShow.join(','))})
    }
  }
  isVisible(field) {
    if (this.state.columnsToShow.indexOf(field) >= 0) {
      return "table-cell";
    } else {
      return "none";
    }
  }
  getStatuses() {
    getStatuses().then(response => {
      var statuses = []
      for (let option of response.data) {
        var status = { value: option.status, label: option.status };
        statuses.push(status);
      }
      this.setState({ allStatuses: statuses });
    });
  }

  getPropertyTypes(){
    getPropertyTypes().then(response => {
      var propertyTypes = []
      for (let type of response.data) {
        var propertyType = { value: type.property_type, label: type.property_type };
        propertyTypes.push(propertyType);
      }
      this.setState({ allPropertyTypes: propertyTypes });
    });
  }

  handlePageClick(data){
    let selectedPage = data.selected;
    this.setState({selectedPage: selectedPage},()=>this.getLead());
  }

  humanize(str) {
    var frags = str.split('_');
    for (let i = 0; i < frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }

  orderLeads(field){
    let direction = this.state.orderLeadsDirection;
    if(direction == 'ASC'){
      direction = 'DESC';
    } else {
      direction = 'ASC';
    }
    this.setState({orderLeadsBy: field, orderLeadsDirection: direction}, ()=>this.getLead())

  }

  userCanAddLead() {
    let userRoles = cookies.get('userRoles').map((role) => role.role);
    if (userRoles.indexOf('sales_manager') >= 0 || userRoles.indexOf('data_entry') >= 0) {
      return true;
    }
    return false
  }

  goToInitialPage(){
    this.setState({selectedPage: 0})
  }

  getAgents(event) {
    getAgents().then(response => {
      let users = [];
      for(let user of response.data){
        let userToBeAdded = {value: user.id, label: user.email};
        users.push(userToBeAdded);
      }
      this.setState({ allUsers: users });
    });
  }

  userIsManagerOrDataEntry(){
    let userRoles = cookies.get('userRoles').map((role)=>role.role);
    if (userRoles.indexOf('sales_manager') >= 0 || userRoles.indexOf('data_entry') >= 0 ){
      return true;
    }
    return false;
  }

  toggleShowArchived(){
    if(this.state.archivedIsShown == false){
      this.setState({archivedIsShown: true},()=>this.getLead())
    } else {
      this.setState({archivedIsShown: false},()=>this.getLead())
    }
  }

  render() {
    return (
      <div className="container lead-page">


        <div className={"row filter-leads"}>

          
          <div className="col-md-4">
            <div className='input-group '>
              <span className="input-group-addon"><i className="fa fa-search" aria-hidden="true"></i></span>
              <input
                  type="text"
                  className="form-control"
                  placeholder="Search leads"
                  name="search"
                  onBlur={this.handleChange}
                  onChange={this.handleChange}
                />
            </div>
          </div>


          <div className='col-md-4'>
            <div id='status-select'>
              <Select
                name="form-field-name"
                value={this.state.selectedStatuses}
                onChange={(data)=>this.handleSelectChange(data,'selectedStatuses')}
                options={this.state.allStatuses}
                multi={true}
                placeholder="Lead Status"
              />
            </div>
          </div>

          <div className="form-group col-md-4">
            <div id='property-type-select'>
              <Select
                name="property_type"
                value={this.state.selectedPropertyTypes}
                onChange={(data)=>this.handleSelectChange(data,'selectedPropertyTypes')}
                options={this.state.allPropertyTypes}
                multi={true}
                placeholder="Property type"
              />
            </div>
          </div>

          <div className="col-md-3">
            <div id="datepic" >
              <div id="reportrange" >
                <i className=" fa fa-calendar fa-lg" />&nbsp;
                <a> {
                  this.state.start_Date ? 
                  
                    <div  className="filter-date">
                      <span style={{color:'gray'}}>Date of first contact</span> <br/>
                      {this.state.start_Date}  
                      <i className="fa fa-arrows-h"></i> 
                      {this.state.end_date} </div>: 
                  'Date of first contact'
                  } </a>
              </div>
            </div>
  
            <div id="reportrangeDateOftrans" >
              <div id="dateoftrans">
                <i className="fa fa-calendar fa-lg" />&nbsp;
                <a> {
                  this.state.start_DateOfTrans ? 
                  <div className="filter-date">
                    <span style={{color:'gray'}}>Date of transaction</span> <br/>
                    {this.state.start_DateOfTrans}
                    <i className="fa fa-arrows-h"></i> 
                    {this.state.end_DateofTrans} </div> : 
                  'Date of transaction'
                  } </a>
              </div>
            </div>
          </div>

          <div className="form-group budget-group col-md-3">
            <div className={"col-md-8"}>
              <input
                id="inputbudgetfrom"
                className="form-control"
                type="number"
                name="budgetFrom"
                onChange={this.handleChange}
                placeholder="Budget from"
              />
              <input
                id="inputbudgetto"
                className="form-control"
                type="number"
                name="budgetTo"
                onChange={this.handleChange}
                placeholder="Budget to" />
            </div>
            <div className={"col-md-4"}>
              <div className="input-group-btn" >
                <select className="currency-dropdown form-control"  name="budget_currency" onChange={this.handleChange}>
                  <option value="EGP">EGP</option>
                  <option value="USD">USD</option>
                  <option value="EUR">EUR</option>
                </select>
              </div>
            </div>
          </div>

          <div className="form-group unit-value-group col-md-3">
           <div className="col-md-8">
              <input 
              id="unit-value-from" 
                className="form-control" 
                type="number" 
                name="unit_vlueFrom" 
                onChange={this.handleChange} 
                placeholder="Unit value from" />
            
            
              <input 
                id="unit-value-to" 
                className="form-control" 
                type="number" 
                name="unit_valueTO" 
                onChange={this.handleChange} 
                placeholder="Unit value to" />
           </div>

            <div className={"col-md-4"}>
              <div className="input-group-btn" >
                <select className="currency-dropdown form-control" name="unit_value_currency" onChange={this.handleChange}>
                  <option value="EGP">EGP</option>
                  <option value="USD">USD</option>
                  <option value="EUR">EUR</option>
                </select>
              </div>
            </div>  
          </div>   

          <div className="col-md-3">
            <div id="buyerstyle" className="custom-checkbox">
              <input
                type="checkbox"
                name="buyer"
                id="buyer"
                onChange={this.handleChange}
              />
              <label htmlFor="buyer">Buyer</label>
            </div>
            <div id="sellerstyle" className="custom-checkbox">
              <input
                type="checkbox"
                name="seller"
                id="seller"
                onChange={this.handleChange}
              />
              <label htmlFor="seller">Seller</label>
            </div>

          </div>

          {this.userIsManagerOrDataEntry()?
            <div className="form-group col-md-4">
            <div>
              <Select
                name="assigned_user"
                value={this.state.selectedAssignedUsers}
                onChange={(data)=>this.handleSelectChange(data,'selectedAssignedUsers')}
                options={this.state.allUsers}
                multi={true}
                placeholder="Assigned user"
              />
            </div>
            </div> :
            ''
          }
          
          {/*{this.state.archivedIsShown? 
            <button className="btn btn-danger pull-right" onClick={this.toggleShowArchived}>Hide Archived</button> :
            <button className="btn btn-info pull-right" onClick={this.toggleShowArchived}>Show Archived</button>
          }*/}
          
          
          
          <div className="col-md-12">
            <h4>Showing {this.state.numberOfSearch} leads of total {this.state.totalLeadCount}</h4>
          </div>

        </div>

        {(this.state.leads.length <= 0) ?
          <h4>No leads to show</h4> :
          (<div >
            <div style={{ 'paddingTop': '10px' }}>
              <i className="fa fa-wrench pull-right" onClick={this.toggleTableOptions}></i>
              {
                (this.state.tableOptionsIsOpened) ?
                  <TableOptions columns={LeadFields}
                    selectedColumns={this.state.columnsToShow}
                    handleSelection={this.handleColumnSelection}
                    toggleShowArchived={this.toggleShowArchived}
                    archivedVisibility={this.state.archivedIsShown} /> : <p></p>
              }
            </div>



            <table className="table table-striped" id="leads-table" >   
              <thead>
                <tr>
                  {LeadFields.map((field)=>
                    <th key={LeadFields.indexOf(field)}
                        style={{ display: this.isVisible(field) }}>
                      {this.humanize(field)}
                      {
                        field == 'property_type' || field == 'buyer/seller'?
                        '':
                        (this.state.orderLeadsBy == field ?
                        <span 
                          onClick={()=>this.orderLeads(field)} 
                          className={this.state.orderLeadsDirection=='ASC'?
                            "fa fa-caret-up fa-lg" : "fa fa-caret-down fa-lg" }>
                        </span> : 
                        <span onClick={()=>this.orderLeads(field)} className="fa fa-sort fa-lg"></span>)
                      }
                    </th>
                    )}
                  
                </tr>
              </thead>
              <tbody>
                {this.state.leads
                  .map(l => (<Lead
                    lead={l}
                    deleteLead={() => this.deleteLead(l.id)}
                    key={l.id}
                    columnsToShow={this.state.columnsToShow}
                  />
                  ))}
              </tbody>
            </table>
            
          </div>)}
          <ReactPaginate 
                  previousLabel={"previous"}
                       nextLabel={"next"}
                       breakLabel={<a href="">...</a>}
                       breakClassName={"break-me"}
                       pageCount={this.state.totalLeadCount/this.state.perPage}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination"}
                       subContainerClassName={"pages pagination"}
                       activeClassName={"active"}
                       forcePage={this.state.selectedPage}
            />

      </div>
    );
  }
}


export default Leads;
